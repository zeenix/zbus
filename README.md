# WARNING: Personal clone.

**Main repository moved to the [canonical D-Bus space](https://gitlab.freedesktop.org/dbus/zbus) and this is now just a personal clone.**
